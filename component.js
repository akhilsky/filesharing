var uuid 	= require('uuid'),
Promise 	= require("bluebird"),
	fs 		= Promise.promisifyAll(require("fs"));


var online_users = []; 
var connection  = false;
module.exports = function(io){
	if(!connection){
	io.on('connection', function (socket) {
				console.log('connected');
				socket.on('adduser',function(username){
			        var ip = socket.request.headers['x-forwarded-for'] || socket.request.connection.remoteAddress;
			        var user_port = socket.request.connection.remotePort; 
			        console.log('New user connected: '+ username +' from: '+ ip);
			        socket.room = 'rockers';
				    socket.join('rockers');
			        if (user_port)
			            console.log('Port: '+ user_port); 

			        
			       if ( online_users.indexOf(username) !== -1)
			        {
			            socket.emit('name taken');
			        }
			        else
			        {
			            user = new User(username,ip,user_port,socket);
			            online_users.push(username);  // add user to list of online users

			            console.log(username + ' just signed in!');
			            console.log('Number of users online: '+ online_users.length);
			            console.log('Online users:' + online_users);

			            var total_users = online_users.length;
			            socket.broadcast.emit('getusrcount',total_users);


			            // notify everyone else
			            socket.broadcast.emit('message',{'from':'SERVER','content':''+ username +' just signed in'});
			        } 
				});

			
				socket.on('upload-file',function(file,fname,ext,username){
					var newfilename = uuid.v4();
			  		 fs.writeFileAsync(__dirname+'/uploads/'+newfilename+"."+ext, file).then(function(){
			  			
			  		}).then(function(){
			  			var data={
			  					"fname":fname,
			  					"newname":newfilename+"."+ext,
			  					"sender":username
						};

			  			io.sockets.to('rockers').emit('get_message', data);
			  		}).catch(function(error){
						 console.error(error);
					});

				});
	});
}

	//Whenever someone disconnects this piece of code executed
	io.sockets.on('disconnect', function (socket) {
	    console.log('A user disconnected');
	    socket.removeAllListeners('send message');
        socket.removeAllListeners('disconnect');
        io.removeAllListeners('connection');
	});
}

	function User(username,peers,ip,port,socket){
	    this.username = username; 
	    this.ip = ip;               
	    this.port = port;
	    this.socket = socket;   
	}

