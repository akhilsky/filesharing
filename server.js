var Promise 	= require("bluebird"),
	fs 			= Promise.promisifyAll(require("fs")),
    http 		= require('http'),
    socketio 	= require('socket.io'),
	url 		= require('url'),
	mime 		= require('mime');


var server = http.createServer(function(req, res) {
	var url_parts = url.parse(req.url);

	if(req.method ==="GET" && req.url === "/"){
		res.writeHead(200, { 'Content-type': 'text/html'});
    	res.end(fs.readFileSync(__dirname + '/index.html'));
	}
	else if(req.method ==="GET" && req.url === "/client"){
		res.writeHead(200, { 'Content-type': 'text/html'});
    	res.end(fs.readFileSync(__dirname + '/client.html'));
	}
	else if(url_parts.pathname = "download"){
		var downloadquery = url_parts.query;
		if(downloadquery){
			var filename  = downloadquery.split("=").pop();
			path = './uploads/' + filename;	
			var type = mime.lookup(path);
			var file = fs.createReadStream(path);
			var stat = fs.statSync(path);
			res.setHeader('Content-Length', stat.size);
			res.setHeader('Content-Type', type);
			res.setHeader('Content-Disposition', 'attachment; filename='+filename);
			file.pipe(res);
		}else{
			res.end("invalid download url, please try again.");
		}
		
	}
   
}).listen(8080, function() {
    console.log('Listening at: http://localhost:8080');
});

var io = socketio.listen(server);
require('./component') (io);

process.on('uncaughtException', function (err) {
  console.error((new Date).toUTCString() + ' uncaughtException:', err.message)
  console.error(err.stack)
  process.exit(1)
})
	